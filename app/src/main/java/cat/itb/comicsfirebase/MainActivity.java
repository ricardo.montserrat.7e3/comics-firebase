package cat.itb.comicsfirebase;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;

import com.firebase.ui.database.FirebaseRecyclerOptions;

import cat.itb.comicsfirebase.Adapter.ComicAdapter;
import cat.itb.comicsfirebase.Adapter.SwipeToDeleteCallback;
import cat.itb.comicsfirebase.Database.DatabaseComics;
import cat.itb.comicsfirebase.Model.Comic;

public class MainActivity extends AppCompatActivity {

    public static DatabaseComics databaseComics = new DatabaseComics();
    public static ComicAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        databaseComics.initialize();

        RecyclerView recyclerView = findViewById(R.id.recycler_comics);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        FirebaseRecyclerOptions<Comic> options = new FirebaseRecyclerOptions
                .Builder<Comic>()
                .setQuery(databaseComics.databaseReference, Comic.class).build();

        adapter = new ComicAdapter(options);
        recyclerView.setAdapter(adapter);

        new ItemTouchHelper(new SwipeToDeleteCallback()).attachToRecyclerView(recyclerView);

        findViewById(R.id.add_activity_button).setOnClickListener(x -> startActivity(new Intent(MainActivity.this, AddEditComic.class)));
    }

    @Override
    protected void onStart()
    {
        super.onStart();
        adapter.startListening();
    }

    @Override
    protected void onStop()
    {
        super.onStop();
        adapter.stopListening();
    }
}