package cat.itb.comicsfirebase.Model;

public class Comic
{
    private String id, title, author, company;
    private float rating;

    public Comic() {
    }

    public Comic(String id, String title, String author, String company, float rating) {
        this.id = id;
        this.title = title;
        this.author = author;
        this.company = company;
        this.rating = rating;
    }

    public Comic(String title, String author, String company, float rating) {
        this.title = title;
        this.author = author;
        this.company = company;
        this.rating = rating;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }
}
