package cat.itb.comicsfirebase.Database;

import androidx.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import cat.itb.comicsfirebase.Model.Comic;

public class DatabaseComics
{
    public FirebaseDatabase database;
    public DatabaseReference databaseReference;

    public void initialize()
    {
        database = FirebaseDatabase.getInstance();
        databaseReference = database.getReference("Comics");
    }

    public String updateDatabase(Comic newComic)
    {
        String id = newComic.getId();
        if(id == null || id.isEmpty())
        {
            id = databaseReference.push().getKey();
            newComic.setId(id);
        }
        assert id != null;
        databaseReference.child(id).setValue(newComic);
        return id;
    }

    public void remove(String id) { databaseReference.child(id).removeValue(); }

    public void readComic(String comic_id, GetDataListener listener)
    {
        databaseReference.child(comic_id).addListenerForSingleValueEvent(new ValueEventListener()
        {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) { listener.onSuccess(dataSnapshot.getValue(Comic.class)); databaseReference.removeEventListener(this);}

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) { }
        });
    }

    public interface GetDataListener
    {
        void onSuccess(Comic value);
    }
}
