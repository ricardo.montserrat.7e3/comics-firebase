package cat.itb.comicsfirebase;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import cat.itb.comicsfirebase.Model.Comic;

public class AddEditComic extends AppCompatActivity
{
    private EditText titleEdit, authorEdit, companyEdit;
    private RatingBar ratingBar;

    private Comic workingOnComic;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_edit_comic);

        titleEdit = findViewById(R.id.title_edit);
        authorEdit = findViewById(R.id.author_edit);
        companyEdit = findViewById(R.id.company_edit);

        Button submitButton = findViewById(R.id.submit_button);
        ratingBar = findViewById(R.id.rating_bar);

        Bundle values = getIntent().getExtras();
        if(values != null)
        {
            submitButton.setText(getResources().getString(R.string.edit));
            MainActivity.databaseComics.readComic(values.getString("comic_id"), comic ->
            {
                workingOnComic = comic;

                ((TextView)findViewById(R.id.title_title)).setText(getResources().getString(R.string.edit));
                titleEdit.setText(workingOnComic.getTitle());
                authorEdit.setText(workingOnComic.getAuthor());
                companyEdit.setText(workingOnComic.getCompany());

                ratingBar.setRating(workingOnComic.getRating());
            });
        }
        else workingOnComic = new Comic();

        submitButton.setOnClickListener(x -> updateComic());
    }

    private void updateComic()
    {
        String author = authorEdit.getText().toString(), title = titleEdit.getText().toString();
        if(!author.isEmpty() && !title.isEmpty())
        {
            workingOnComic.setAuthor(author);
            workingOnComic.setTitle(title);
            workingOnComic.setCompany(companyEdit.getText().toString());

            workingOnComic.setRating(ratingBar.getRating());

            System.out.println(MainActivity.databaseComics.updateDatabase(workingOnComic) + " Succesfully Updated!");
            finish();
        } else Toast.makeText(this, "Please Fill the Author and Title fields", Toast.LENGTH_SHORT).show();
    }
}