package cat.itb.comicsfirebase.Adapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import cat.itb.comicsfirebase.MainActivity;

public class SwipeToDeleteCallback extends ItemTouchHelper.SimpleCallback
{
    public SwipeToDeleteCallback() { super(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT); }

    @Override
    public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) { return false; }

    @Override
    public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) { MainActivity.databaseComics.remove(MainActivity.adapter.getItem(viewHolder.getAdapterPosition()).getId()); }
}