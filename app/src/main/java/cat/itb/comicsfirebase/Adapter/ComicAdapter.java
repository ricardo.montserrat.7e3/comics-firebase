package cat.itb.comicsfirebase.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;

import cat.itb.comicsfirebase.AddEditComic;
import cat.itb.comicsfirebase.Model.Comic;
import cat.itb.comicsfirebase.R;

public class ComicAdapter extends FirebaseRecyclerAdapter<Comic, ComicAdapter.ComicViewHolder>
{
    public ComicAdapter(@NonNull FirebaseRecyclerOptions<Comic> options) { super(options); }

    @Override
    protected void onBindViewHolder(@NonNull ComicViewHolder holder, int position, @NonNull Comic model)
    {
        holder.title.setText(model.getTitle());
        holder.author.setText(model.getAuthor());
        holder.company.setText(model.getCompany());
        holder.rating.setText(String.valueOf(model.getRating()));

        holder.itemView.setOnClickListener(x ->
        {
            Context itemContext = holder.itemView.getContext();

            Intent intent = new Intent(itemContext, AddEditComic.class);
            intent.putExtra("comic_id", model.getId());

            itemContext.startActivity(intent);
        });
    }

    @NonNull
    @Override
    public ComicViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        return new ComicViewHolder(LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.comic_view_layout, parent, false));
    }

    public static class ComicViewHolder extends RecyclerView.ViewHolder {
        TextView title, author, company, rating;

        public ComicViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.comic_title);
            author = itemView.findViewById(R.id.comic_author);
            company = itemView.findViewById(R.id.comic_company);
            rating = itemView.findViewById(R.id.comic_rating);
        }
    }
}
